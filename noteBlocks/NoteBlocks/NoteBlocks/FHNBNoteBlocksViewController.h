//
//  FHNBNoteBlocksViewController.h
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHNBBlock.h"
#import "FHNBNewNoteView.h"
#import "Note.h"

@interface FHNBNoteBlocksViewController : UIViewController

@property (nonatomic) FHNBBlock *currentBlock;
@property (nonatomic) FHNBNewNoteView *addNote;
@property (nonatomic) NSMutableArray *blockList;
@property (nonatomic) Note *currentNote;


@end
