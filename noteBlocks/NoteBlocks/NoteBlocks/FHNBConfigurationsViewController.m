//
//  FHNBConfigurationsViewController.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 3/4/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBConfigurationsViewController.h"

@interface FHNBConfigurationsViewController ()
@property (strong, nonatomic) IBOutlet UISwitch *gravity;
@property (strong, nonatomic) IBOutlet UISwitch *sound;

@end

@implementation FHNBConfigurationsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustSetting];
    
	// Do any additional setup after loading the view.
}
- (void) adjustSetting
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gravity"] isEqualToString: @"on"])
    {
        [_gravity setOn:YES animated:YES];
    }
    else
    {
        [_gravity setOn:NO animated:YES];
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"sound"] isEqualToString: @"on"])
    {
        [_sound setOn:YES animated:YES];
    }
    else
    {
        [_sound setOn:NO animated:YES];
    }
    
    
}

- (IBAction)gravity:(UISwitch *)sender {
}

- (IBAction)sound:(UISwitch *)sender {
}

- (IBAction)confirmAction:(UIButton *)sender {[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sound"];
    if (_sound.on)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"sound"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"off" forKey:@"sound"];
    }
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"gravity"];
    if (_gravity.on)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"gravity"];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"off" forKey:@"gravity"];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
