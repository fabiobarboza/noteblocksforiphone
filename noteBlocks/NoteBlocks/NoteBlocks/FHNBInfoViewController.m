//
//  FHNBInfoViewController.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 3/4/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBInfoViewController.h"

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

@interface FHNBInfoViewController ()
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UILabel *label;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation FHNBInfoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustSettings];
}

- (void) adjustSettings
{
    if (isiPhone5)
    {
        [_textView setFrame: CGRectMake(20, 237, 280, 192)];
        [_label setFrame: CGRectMake(72, 469, 177, 21)];
        [_imageView setFrame: CGRectMake(72, 498, 177, 55)];
    }
    else
    {
        [_textView setFrame: CGRectMake(20, 217, 280, 142)];
        [_label setFrame: CGRectMake(72, 395, 177, 21)];
        [_imageView setFrame: CGRectMake(72, 415, 177, 55)];
    }
    
}


@end
