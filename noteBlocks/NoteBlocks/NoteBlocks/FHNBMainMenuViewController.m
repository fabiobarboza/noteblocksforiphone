//
//  FHNBMainMenuViewController.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 3/4/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBMainMenuViewController.h"

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

@interface FHNBMainMenuViewController ()
@property (strong, nonatomic) IBOutlet UIButton *notes;

@property (strong, nonatomic) IBOutlet UIButton *configurations;
@property (strong, nonatomic) IBOutlet UIButton *informations;

@end

@implementation FHNBMainMenuViewController
{
    int firstButtonPositionY;
    int buttonSizeWidth;
    int buttonSizeHeight;
    int buttonSpaceBetweenTwoDifferentsButtons;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustSettings];
    [self configureView];
}

- (void) adjustSettings
{
    if (isiPhone5)
    {
        firstButtonPositionY = 237;
        buttonSizeWidth = 120;
        buttonSizeHeight = 80;
        buttonSpaceBetweenTwoDifferentsButtons = 95;
    }
    else
    {
        firstButtonPositionY = 215;
        buttonSizeWidth = 115;
        buttonSizeHeight = 75;
        buttonSpaceBetweenTwoDifferentsButtons = 85;
    }
    [_notes setFrame: CGRectMake(100, firstButtonPositionY, buttonSizeWidth, buttonSizeHeight)];
    _configurations.frame = CGRectMake(100, firstButtonPositionY + buttonSpaceBetweenTwoDifferentsButtons, buttonSizeWidth, buttonSizeHeight);
    _informations.frame = CGRectMake(100, firstButtonPositionY + buttonSpaceBetweenTwoDifferentsButtons * 2, buttonSizeWidth, buttonSizeHeight);
    
}

- (void)configureView
{
    self.title = @"Menu";
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"gravity"] == nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"gravity"];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"sound"] == nil)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"sound"];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end
