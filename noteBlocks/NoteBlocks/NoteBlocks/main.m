//
//  main.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/20/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FHNBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FHNBAppDelegate class]));
    }
}
