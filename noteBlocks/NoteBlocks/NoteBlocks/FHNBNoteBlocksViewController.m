//
//  FHNBNoteBlocksViewController.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBNoteBlocksViewController.h"
#import <CoreMotion/CoreMotion.h>
#import "FHNBChangeColorViewController.h"
#import "FHNBAppDelegate.h"
#include <stdlib.h>
#import <AudioToolbox/AudioServices.h>

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

@interface FHNBNoteBlocksViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) UIDynamicAnimator *animator;
@property (strong, nonatomic) UIGravityBehavior *gravity;
@property (strong, nonatomic) UICollisionBehavior *collision;
@property (strong , nonatomic) UIDynamicItemBehavior *itemB;


@property (nonatomic) int posX;


@end

@implementation FHNBNoteBlocksViewController
{
    NSManagedObjectContext *context;
    CMMotionManager* myMotion;
    BOOL editingBlock;
    NSTimer * timerToCreateBox;
    int objectAtIndexFromCoreData;
    int currentModification; //1 - IF CHANGE BACKGROUND COLOR 2 - IF CHANGE TEXT COLOR
}

//##############################################################################################################

#pragma mark - Load Blocks

- (void) loadAllBLocksFromCoreData
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (objectAtIndexFromCoreData < fetchedObjects.count && fetchedObjects.count > 0)
    {
        FHNBBlock* block = [[FHNBBlock alloc] init];
        block.myNote = (Note *)fetchedObjects[objectAtIndexFromCoreData];
        [block adjustSettingsFromCoreData];
        [self createSettingsToCreateBlock: block];
        objectAtIndexFromCoreData++;
    }
    else
    {
        [timerToCreateBox invalidate];
        [self startAccelerometer];
        _btnAdd.enabled = YES;
    }
    
}


//##############################################################################################################

#pragma mark - View Delegates

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self startApp];
    [self loadAllBLocksFromCoreData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    if ([timerToCreateBox isValid])
    {
        [timerToCreateBox invalidate];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"changeColorSegue"])
    {
        FHNBChangeColorViewController *changeColorVC = segue.destinationViewController;
        changeColorVC.currentModification = currentModification;
        changeColorVC.myBlock = _currentBlock;
        
        
        changeColorVC.creationView = _addNote;
        
        if (currentModification == 1)
        {
            changeColorVC.buttonChanged = _addNote.btnBackgroundColor;
        }
        else
        {
            changeColorVC.buttonChanged = _addNote.btnFontColor;
        }
    }
}


//##############################################################################################################

#pragma mark - Starting App Settings

- (void)startApp
{
    
    //Initialize Settings
    [_btnAdd addTarget:self action:@selector(createNewNote) forControlEvents:UIControlEventTouchUpInside];
    [_btnBack addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    editingBlock = false;
    _btnAdd.enabled = NO;
    objectAtIndexFromCoreData = 0;
    timerToCreateBox = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                        target: self
                                                      selector:@selector(loadAllBLocksFromCoreData)
                                                      userInfo: nil repeats:YES];
    
    //Initialize Context
    context = [(FHNBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    //Initialize all behaviors
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    _gravity = [[UIGravityBehavior alloc] init];
    [_animator addBehavior:_gravity];
    _collision = [[UICollisionBehavior alloc] init];
    _collision.translatesReferenceBoundsIntoBoundary = YES;
    [_animator addBehavior:_collision];
    _itemB = [[UIDynamicItemBehavior alloc] init];
    _itemB.elasticity = 0.4f;
    [_animator addBehavior:_itemB];
}

- (void)startAccelerometer
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gravity"] isEqualToString:@"on"])
    {
        myMotion = [[CMMotionManager alloc] init];
        myMotion.accelerometerUpdateInterval = .1;
        if ([myMotion isAccelerometerAvailable])
        {
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [myMotion startAccelerometerUpdatesToQueue:queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _gravity.gravityDirection = CGVectorMake(accelerometerData.acceleration.x, - accelerometerData.acceleration.y);
                });
            }];
        }
        else
            NSLog(@"not active");
        
    }

}


//##############################################################################################################

#pragma mark - Creating New Block

- (void)createNewNote {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (editingBlock == NO && fetchedObjects.count > 9)
    {
        UIAlertView* alert;
        alert = [[UIAlertView alloc] initWithTitle:@"Ops!" message:@"Only 10 notes are available at the same time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [self showNewNoteView];
        
        [_btnAdd setTitle:@"Confirm" forState:UIControlStateNormal];
        [_btnBack setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnAdd removeTarget:self action:@selector(createNewNote) forControlEvents:UIControlEventTouchUpInside];
        [_btnAdd addTarget:self action:@selector(addBlock) forControlEvents:UIControlEventTouchUpInside];
        [_btnBack removeTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
        [_btnBack addTarget:self action:@selector(removeNewNoteView) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void) createSettingsToCreateBlock : (FHNBBlock *) nextBlock
{
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    
    [_blockList addObject:nextBlock];
    
    [nextBlock setFrame:CGRectMake([self getRandomNumberBetween:0 to:200], nextBlock.frame.origin.y, nextBlock.frame.size.width, nextBlock.frame.size.height)];
    [nextBlock addGestureRecognizer:longPress];
    
    [self.view addSubview:nextBlock];
    [_gravity addItem:nextBlock];
    [_animator addBehavior:_gravity];
    [_collision addItem:nextBlock];
    [_animator addBehavior:_collision];
    [_itemB addItem:nextBlock];
    _itemB.elasticity = 0.4f;
    [_animator addBehavior:_itemB];
    
}

-(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
    
}

- (void)addBlock
{
    if (editingBlock == NO)
    {
        FHNBBlock *nextBlock = [_addNote createNewBlock];
        [self createSettingsToCreateBlock:nextBlock];
    }
    else
    {
        if (_addNote.noteBackgroundColor != nil)
            _currentBlock.myNote.background = _addNote.noteBackgroundColor;
        if (_addNote.noteFontColor != nil)
            _currentBlock.myNote.fontColor = _addNote.noteFontColor;
        _currentBlock.myNote.title = _addNote.noteTitle.text;
        _currentBlock.myNote.text = _addNote.noteText.text;
        _currentBlock.title.text = _currentBlock.myNote.title;
        if (_addNote.blockBackgroundColor != nil)
            _currentBlock.backgroundColor = _addNote.blockBackgroundColor;
        if (_addNote.blockFontColor != nil)
            _currentBlock.title.textColor = _addNote.blockFontColor;
        [self editBlock];
    }
    
    [self removeNewNoteView];
    
    [_btnAdd setTitle:@"Add" forState:UIControlStateNormal];
    [_btnBack setTitle:@"Menu" forState:UIControlStateNormal];
    
    [_btnAdd removeTarget:self action:@selector(addBlock) forControlEvents:UIControlEventTouchUpInside];
    [_btnAdd addTarget:self action:@selector(createNewNote) forControlEvents:UIControlEventTouchUpInside];
    [_btnBack removeTarget:self action:@selector(removeNewNoteView) forControlEvents:UIControlEventTouchUpInside];
    _addNote = nil;
    _currentBlock = nil;
    [self.view endEditing:YES];
    [_btnBack addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
}

//##############################################################################################################

#pragma mark - Editing Block

- (void)editBlock
{
    [_currentBlock saveNote];
    
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [_currentBlock addGestureRecognizer: longPress];
    editingBlock = NO;
    
}

- (void)openView:(UIButton*)button
{
    
    editingBlock = YES;
    
    [self createNewNote];
    
    FHNBBlock *block = (FHNBBlock *)[button superview];
    
    [[NSUserDefaults standardUserDefaults]setObject:block.myNote.background forKey:@"currentBackgroundColor"];
    
    [[NSUserDefaults standardUserDefaults]setObject:block.myNote.fontColor forKey:@"currentFontColor"];
    
    _currentBlock = block;
    _addNote.btnBackgroundColor.backgroundColor = block.backgroundColor;
    [_addNote.btnFontColor setTitleColor:block.title.textColor forState:UIControlStateNormal];
    
    _addNote.noteTitle.text = block.myNote.title;
    _addNote.noteText.text = block.myNote.text;
    
    
    for (UIView *view in block.subviews)
    {
        if ([view isKindOfClass:[UIButton class]])
        {
            [view removeFromSuperview];
        }
    }
}

- (void)longPress:(id)object
{
    _currentBlock = (FHNBBlock *)((UILongPressGestureRecognizer*)object).view;
    
    for (UIGestureRecognizer* gesture in _currentBlock.gestureRecognizers)
    {
        [_currentBlock removeGestureRecognizer: gesture];
    }
    
    UIButton* removeButton = [[UIButton alloc] initWithFrame: CGRectMake(-2, -2, 30, 30)];
    [removeButton addTarget:self action:@selector(removeView:) forControlEvents:UIControlEventTouchUpInside];
    UIButton* editButton = [[UIButton alloc] initWithFrame: CGRectMake(90, 53, 30,30)];
    [editButton addTarget:self action:@selector(openView:) forControlEvents:UIControlEventTouchUpInside];
    [removeButton setBackgroundImage:[UIImage imageNamed:@"alt.png"] forState:UIControlStateNormal];
    [editButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        [_currentBlock addSubview: removeButton];
        [_currentBlock addSubview: editButton];
    }];
    
}



//##############################################################################################################

#pragma mark - Show/Hide NewNoteView

- (void)showNewNoteView
{
    
    if (!_addNote)
        _addNote = [[FHNBNewNoteView alloc] init];
    if (isiPhone5)
    {
        [_addNote createSettingsForIphone5];
    }
    else
    {
        [_addNote createSettingsForIphone4];
    }
    
    [_addNote.btnBackgroundColor addTarget:self action:@selector(changeBackgroundColor) forControlEvents:UIControlEventTouchUpInside];
    [_addNote.btnFontColor addTarget:self action:@selector(changeFontColor) forControlEvents:UIControlEventTouchUpInside];
    _addNote.center = CGPointMake(self.view.frame.size.width /2, -450);
    
    [self.view addSubview: _addNote];
    
    self.btnAdd.enabled = NO;
    self.btnBack.enabled = NO;
    
    [UIView animateWithDuration:1.0 animations:^{
        _addNote.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2 + 60);
    } completion:^(BOOL finished) {
        
        if (finished)
        {
            self.btnAdd.enabled = YES;
            self.btnBack.enabled = YES;
        }
        
    }];
    
}

- (void)removeNewNoteView
{
    self.btnBack.enabled = NO;
    self.btnAdd.enabled = NO;
    
    _addNote.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2);
    [UIView animateWithDuration:1.0 animations:^{
        _addNote.center = CGPointMake(self.view.frame.size.width /2, -450);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2.0 animations:^{
            [_addNote removeFromSuperview];
            self.btnBack.enabled = YES;
            self.btnAdd.enabled = YES;
        }];
    }];
    UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [_currentBlock addGestureRecognizer: longPress];
    
    [_btnAdd setTitle:@"Add" forState:UIControlStateNormal];
    [_btnBack setTitle:@"Menu" forState:UIControlStateNormal];
    
    [_btnAdd removeTarget:self action:@selector(addBlock) forControlEvents:UIControlEventTouchUpInside];
    [_btnAdd addTarget:self action:@selector(createNewNote) forControlEvents:UIControlEventTouchUpInside];
    [_btnBack removeTarget:self action:@selector(removeNewNoteView) forControlEvents:UIControlEventTouchUpInside];
    [_btnBack addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
}



//##############################################################################################################

#pragma mark - Buttons Actions

- (void)changeBackgroundColor
{
    currentModification = 1;
    [self performSegueWithIdentifier:@"changeColorSegue" sender:self];
}

- (void)changeFontColor
{
    currentModification = 2;
    [self performSegueWithIdentifier:@"changeColorSegue" sender:self];
}

- (void)backButton:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//##############################################################################################################

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.addNote.frame;
        f.origin.y = +65.0f;  //set the -35.0f to your required value
        self.addNote.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.addNote.frame;
        f.origin.y = +210.0f;
        self.addNote.frame = f;
    }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    if (_addNote == nil)
    {
        UILongPressGestureRecognizer* longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
        [_currentBlock addGestureRecognizer: longPress];
    }else
    {
        [_addNote.noteText resignFirstResponder];
        [_addNote.noteTitle resignFirstResponder];
    }
    
    if (_currentBlock != nil)
    {
        for (UIButton* myButton in _currentBlock.subviews)
        {
            if ([myButton isKindOfClass:[UIButton class]])
            {
                [myButton removeFromSuperview];
            }
        }
        _currentBlock = nil;
    }
    
}

//##############################################################################################################

#pragma mark - Removing Block


- (void)removeView:(UIButton*)button
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"sound"] isEqualToString:@"on"])
        [self playSound];
    UIView* aView = [button superview];
    UIPushBehavior* push = [[UIPushBehavior alloc] initWithItems:@[aView] mode:UIPushBehaviorModeInstantaneous];
    push.magnitude = -150.3;
    push.angle = 150.0;
    [_animator addBehavior: push];
    
    [UIView animateWithDuration:0.9 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAllowUserInteraction animations:^{
        
    } completion:^(BOOL finished) {
        
        if (finished) {
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                for (UIButton* myButton in aView.subviews)
                {
                    [myButton removeFromSuperview];
                }
                [aView setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bola.png"]]];
                
                
            } completion:^(BOOL finished) {
                if (finished)
                {
                    NSError *error;
                    [aView setBackgroundColor: [UIColor redColor]];
                    [context deleteObject: ((FHNBBlock *) aView).myNote];
                    [context save:&error];
                    [aView removeFromSuperview];
                    [_collision removeItem:aView];
                }
                
            }];
        }
    }];
}

-(void) playSound {
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"sound2" ofType:@"wav"];
    SystemSoundID soundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
    AudioServicesPlaySystemSound (soundID);
    //[soundPath release];
}
//##############################################################################################################

@end
