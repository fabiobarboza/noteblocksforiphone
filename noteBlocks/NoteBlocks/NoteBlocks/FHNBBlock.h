//
//  FHNBBlock.h
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@interface FHNBBlock : UIView

@property (nonatomic) UILabel *title;
@property (nonatomic) Note *myNote;

- (void)adjustSettings;
- (void)createMyNote;
- (void)adjustSettingsFromCoreData;
- (void)saveNote;
- (instancetype)initWithNote:(Note *)aNote;
@end
