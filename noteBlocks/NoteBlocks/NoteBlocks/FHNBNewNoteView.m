//
//  FHNBNewNoteView.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBNewNoteView.h"
#import "FHNBAppDelegate.h"


@implementation FHNBNewNoteView
{
    int noteTextHeight;
    int btnColorsPositionY;
    int btnSize;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype) initWithOutNewNote
{
    self = [super init];
    if (self) {
        [self createComponents];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void) createSettingsForIphone5
{
    self.frame = CGRectMake(40, 120, 250, 280);
    self.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:1.0 alpha:1];
    noteTextHeight = 150;
    btnSize = 30;
    btnColorsPositionY = 240;
    [self createComponents];
}

- (void) createSettingsForIphone4
{
    self.frame = CGRectMake(40, 120, 250, 185);
    self.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:1.0 alpha:1];
    noteTextHeight = 70;
    btnColorsPositionY = 159;
    btnSize = 25;
    [self createComponents];
}

- (void)createComponents
{
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(-6, -6, self.frame.size.width + 12, self.frame.size.height + 20)];
    backgroundImage.image = [UIImage imageNamed:@"box.png"];
    [self addSubview:backgroundImage];
    
    
    int xPosition = 25;
    
    self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.9];
    
    UIColor *componentsBGColor = [UIColor whiteColor];
    UIColor *borderColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:1];
    float borderWidth = 1.0;
    float borderRarius = 5.0;
    
    UILabel *viewTitle = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, 15, 200, 30)];
    viewTitle.text = @"New note";
    viewTitle.font = [UIFont systemFontOfSize:20];
    [self addSubview:viewTitle];
    
    _noteTitle = [[UITextField alloc] initWithFrame:CGRectMake(xPosition, 50, 200, 30)];
    _noteTitle.placeholder = @"Note title";
    _noteTitle.backgroundColor = componentsBGColor;
    _noteTitle.layer.borderWidth = borderWidth;
    _noteTitle.layer.cornerRadius = borderRarius;
    _noteTitle.layer.borderColor = borderColor.CGColor;
    _noteTitle.delegate = self;
    [self addSubview:_noteTitle];
    
    _noteText = [[UITextView alloc] initWithFrame:CGRectMake(xPosition, 85, 200, noteTextHeight)];
    _noteText.text = @"Note text";
    _noteText.backgroundColor = componentsBGColor;
    _noteText.layer.borderWidth = borderWidth;
    _noteText.layer.cornerRadius = borderRarius;
    _noteText.layer.borderColor = borderColor.CGColor;
    _noteText.delegate = self;
    [self addSubview:_noteText];
    
    _btnBackgroundColor = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width /2 - 35, btnColorsPositionY, btnSize, btnSize)];
    _btnBackgroundColor.backgroundColor = [UIColor whiteColor];
    _btnBackgroundColor.layer.borderWidth = borderWidth;
    _btnBackgroundColor.layer.cornerRadius = _btnBackgroundColor.frame.size.width / 2;
    _btnBackgroundColor.layer.borderColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1].CGColor;
    [self addSubview:_btnBackgroundColor];
    
    _btnFontColor = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width /2 + 5, btnColorsPositionY, btnSize, btnSize)];
    _btnFontColor.backgroundColor = [UIColor whiteColor];
    _btnFontColor.layer.borderWidth = borderWidth - 0.5;
    [_btnFontColor setTitle:@"A" forState:UIControlStateNormal];
    _btnFontColor.titleLabel.font = [UIFont systemFontOfSize:20];
    [_btnFontColor setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _btnFontColor.layer.cornerRadius = _btnFontColor.frame.size.width / 2;
    _btnFontColor.layer.borderColor = [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1].CGColor;
    [self addSubview:_btnFontColor];
    
}


- (FHNBBlock *)createNewBlock
{
    
    NSManagedObjectContext *context = [(FHNBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    Note *note = (Note *)[NSEntityDescription insertNewObjectForEntityForName:@"Note" inManagedObjectContext:context];
    
    note.title = (NSString *)_noteTitle.text;
    note.text = (NSString *)_noteText.text;
    note.background = _noteBackgroundColor;
    note.fontColor = _noteFontColor;
    [self getNextIdentifier:note];
    NSError *error;
    
    if (![context save:&error])
    {
        NSLog(@"Erro ao salvar: %@", [error localizedDescription]);
    }
    
    FHNBBlock *block = [[FHNBBlock alloc] initWithNote:note];
    return block;
}


- (void)getNextIdentifier:(Note *)note
{
    note.identification = [[NSUserDefaults standardUserDefaults] objectForKey:@"staticIdentifier"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"staticIdentifier"];
    [[NSUserDefaults standardUserDefaults] setObject: [[NSNumber alloc] initWithInt: note.identification.intValue + 1]  forKey:@"staticIdentifier"];
}
- (FHNBBlock *)createCustomBlock
{
    FHNBBlock *myBlock = [[FHNBBlock alloc] init];
    [myBlock createMyNote];
    myBlock.myNote.title = _noteTitle.text;
    myBlock.myNote.text = _noteText.text;
    [myBlock adjustSettings];
    return myBlock;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_noteTitle resignFirstResponder];
    return YES;
}

- (UIColor *)convertColorFromNote : (NSString *) colorType
{
    NSArray *myWords;
    if ([colorType isEqualToString: @"background"])
    {
        myWords = [_noteBackgroundColor componentsSeparatedByString:@" "];
    }
    else if ([colorType isEqualToString:@"fontColor"])
    {
        myWords = [_noteFontColor componentsSeparatedByString:@" "];
    }
    
    NSString *redS = (NSString *)myWords[0];
    int red = redS.intValue;
    NSString *greenS = (NSString *)myWords[1];
    int green = greenS.intValue;
    NSString *blueS = (NSString *)myWords[2];
    int blue = blueS.intValue;
    
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
    return color;
}

#define MAX_LENGTH 10

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= MAX_LENGTH && range.length == 0)
    {
    	return NO; // return NO to not change text
    }
    else
    {return YES;}
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
