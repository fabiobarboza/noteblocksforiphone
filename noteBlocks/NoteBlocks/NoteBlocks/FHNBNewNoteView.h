//
//  FHNBNewNoteView.h
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FHNBBlock.h"


@interface FHNBNewNoteView : UIView  <UITextFieldDelegate, UITextViewDelegate>


@property (nonatomic) UITextField *noteTitle;
@property (nonatomic) UITextView *noteText;
@property (nonatomic) UIButton *btnBackgroundColor;
@property (nonatomic) UIButton *btnFontColor;
@property (nonatomic) UIColor *blockBackgroundColor;
@property (nonatomic) UIColor *blockFontColor;
@property (nonatomic) NSString *noteBackgroundColor;
@property (nonatomic) NSString *noteFontColor;

- (instancetype) initWithOutNewNote;
- (FHNBBlock *)createCustomBlock;
- (FHNBBlock *)createNewBlock;


- (void) createSettingsForIphone5;
- (void) createSettingsForIphone4;

@end
