//
//  FHNBChangeColorViewController.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/27/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBChangeColorViewController.h"

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

@interface FHNBChangeColorViewController ()
{
    UIColor *currentColor;
}

@property (weak, nonatomic) IBOutlet UIView *simpleView;
@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;

@end

@implementation FHNBChangeColorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createViewSettings];
	[self adjustSettings];
    [self adjustSliders];
}

- (void)createViewSettings
{
    self.simpleView.backgroundColor = [UIColor colorWithRed:_redSlider.value/255.0 green:_greenSlider.value/255.0 blue:_blueSlider.value/255.0 alpha:1];
    self.simpleView.layer.borderColor = self.simpleView.backgroundColor.CGColor;
    currentColor = self.simpleView.backgroundColor;
}


- (void)adjustSliders
{
    NSString *myString;
    if (_currentModification == 1)
        myString = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentBackgroundColor"];
    else
        myString = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentFontColor"];
    NSArray *myWords = [myString componentsSeparatedByString:@" "];
    
    NSString *redS = (NSString *)myWords[0];
    [_redSlider setValue:redS.intValue animated:YES];
    NSString *greenS = (NSString *)myWords[1];
    [_greenSlider setValue:greenS.intValue animated:YES];
    NSString *blueS = (NSString *)myWords[2];
    [_blueSlider setValue:blueS.intValue animated:YES];
    
    self.simpleView.backgroundColor = [UIColor colorWithRed:_redSlider.value/255.0 green:_greenSlider.value/255.0 blue:_blueSlider.value/255.0 alpha:1];
    self.simpleView.layer.borderColor = self.simpleView.backgroundColor.CGColor;
    
    currentColor = self.simpleView.backgroundColor;
    
}


- (void) adjustSettings
{
    if (isiPhone5)
    {
        [_simpleView setFrame:CGRectMake(60, 87, 200, 200)];
        
        _simpleView.layer.borderWidth = 1;
        _simpleView.layer.borderColor = _simpleView.backgroundColor.CGColor;
        _simpleView.layer.cornerRadius = 100;
        [_simpleView clipsToBounds];
        
        [_redLabel setFrame: CGRectMake(20, 307, 280, 21)];
        [_greenLabel setFrame: CGRectMake(20, 377, 280, 21)];
        [_blueLabel setFrame: CGRectMake(20, 447, 280, 21)];
        [_redSlider setFrame: CGRectMake(18, 336, 284, 34)];
        [_greenSlider setFrame: CGRectMake(18, 406, 284, 34)];
        [_blueSlider setFrame: CGRectMake(18, 476, 284, 34)];
    }
    else
    {
        [_simpleView setFrame:CGRectMake(85, 87, 150, 150)];
        _simpleView.layer.borderWidth = 1;
        _simpleView.layer.borderColor = _simpleView.backgroundColor.CGColor;
        _simpleView.layer.cornerRadius = 75;
        [_simpleView clipsToBounds];
        
        [_redLabel setFrame: CGRectMake(20, 267, 280, 21)];
        [_greenLabel setFrame: CGRectMake(20, 337, 280, 21)];
        [_blueLabel setFrame: CGRectMake(20, 407, 280, 21)];
        [_redSlider setFrame: CGRectMake(18, 296, 284, 34)];
        [_greenSlider setFrame: CGRectMake(18, 366, 284, 34)];
        [_blueSlider setFrame: CGRectMake(18, 436, 284, 34)];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    self.simpleView.backgroundColor = [UIColor colorWithRed:_redSlider.value/255.0 green:_greenSlider.value/255.0 blue:_blueSlider.value/255.0 alpha:1];
    self.simpleView.layer.borderColor = self.simpleView.backgroundColor.CGColor;
    if (_currentModification == 1)
    {
        NSString* colorRgb = [NSString stringWithFormat:@"%d %d %d",(int)_redSlider.value, (int) _greenSlider.value, (int) _blueSlider.value];
        _creationView.noteBackgroundColor = colorRgb;
        _creationView.blockBackgroundColor = _simpleView.backgroundColor;
    }
    else
    {
        NSString* colorRgb = [NSString stringWithFormat:@"%d %d %d",(int)_redSlider.value, (int) _greenSlider.value, (int) _blueSlider.value];
        _creationView.noteFontColor = colorRgb;
        _creationView.blockFontColor = _simpleView.backgroundColor;
    }
    currentColor = self.simpleView.backgroundColor;
}

- (IBAction)confirmEdition:(UIButton *)sender {
    if (_currentModification == 1)
    {
        NSString* colorRgb = [NSString stringWithFormat:@"%d %d %d",(int)_redSlider.value, (int) _greenSlider.value, (int) _blueSlider.value];
        _creationView.noteBackgroundColor = colorRgb;
        _creationView.blockBackgroundColor = _simpleView.backgroundColor;
        currentColor = self.simpleView.backgroundColor;
        self.buttonChanged.backgroundColor = currentColor;
    }
    else
    {
        NSString* colorRgb = [NSString stringWithFormat:@"%d %d %d",(int)_redSlider.value, (int) _greenSlider.value, (int) _blueSlider.value];
        _creationView.noteFontColor = colorRgb;
        _creationView.blockFontColor = _simpleView.backgroundColor;
        currentColor = self.simpleView.backgroundColor;
        [self.buttonChanged setTitleColor:currentColor forState:UIControlStateNormal];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelEdition:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
