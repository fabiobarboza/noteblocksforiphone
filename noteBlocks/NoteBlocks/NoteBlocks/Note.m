//
//  Note.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 3/6/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "Note.h"


@implementation Note

@dynamic background;
@dynamic text;
@dynamic title;
@dynamic fontColor;
@dynamic identification;

@end
