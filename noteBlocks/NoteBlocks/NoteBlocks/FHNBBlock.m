//
//  FHNBBlock.m
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/17/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import "FHNBBlock.h"
#import "FHNBAppDelegate.h"

@implementation FHNBBlock

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithNote:(Note *)aNote;
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake((320/2) - 40, 0, 120, 80);
        [self adjustBackgroundImage];
        self.myNote = aNote;
        [self createMyNote];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake((320/2) - 40, 0, 120, 80);
        [self adjustBackgroundImage];
    }
    return self;
}

- (void) createComponents
{
    _title = [[UILabel alloc] initWithFrame:CGRectMake(0, (self.frame.size.height/2) - 20, self.frame.size.width, 40)];
    _title.text = _myNote.title;
    _title.font = [UIFont systemFontOfSize:20];
    _title.textColor = [self convertColorFromNote : @"fontColor"];
    _title.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_title];
}

- (void) createMyNote
{
    self.backgroundColor = [self convertColorFromNote:@"background"];
    [self createComponents];
    
}

- (void)adjustBackgroundImage
{
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(-3, -3, self.frame.size.width + 6, self.frame.size.height + 5)];
    backgroundImageView.image = [UIImage imageNamed:@"box.png"];
    [self addSubview:backgroundImageView];
}

- (void)createBlockTitleWithText:(NSString *)aText andColor:(UIColor *)aColor
{
    _title.text = aText;
    _title.textColor = aColor;
    [self saveNote];
}

- (void) saveNote
{
    NSManagedObjectContext *context = [(FHNBAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    for (Note *note in fetchedObjects)
    {
        if (note.identification == _myNote.identification)
        {
            note.title =  _myNote.title;
            note.text = _myNote.text;
            note.background = _myNote.background;
            note.fontColor = _myNote.fontColor;
        }
    }
    
    if (![context save:&error])
    {
        NSLog(@"Erro ao salvar: %@", [error localizedDescription]);
    }
}
- (void)adjustSettings
{
    self.backgroundColor = [self convertColorFromNote : @"background"];
    [self createBlockTitleWithText:_myNote.title andColor: [self convertColorFromNote : @"fontColor"]];
}

- (void)adjustSettingsFromCoreData
{
    
    self.backgroundColor = [self convertColorFromNote : @"background"];
    for (UIView * view in self.subviews)
    {
        if ([view isKindOfClass:[UILabel class]])
        {
            [view removeFromSuperview];
        }
    }
    _title = [[UILabel alloc] initWithFrame:CGRectMake(0, (self.frame.size.height/2) - 20, self.frame.size.width, 40)];
    _title.text = _myNote.title;
    _title.font = [UIFont systemFontOfSize:20];
    _title.textColor = [self convertColorFromNote : @"fontColor"];
    _title.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_title];
}


- (UIColor *)convertColorFromNote : (NSString *) colorType
{
    NSArray *myWords;
    if ([colorType isEqualToString: @"background"])
    {
        if (_myNote.background != nil)
        myWords = [_myNote.background componentsSeparatedByString:@" "];
        else
            myWords = [[NSArray alloc] initWithObjects:@"255",@"255",@"255",nil];
    }
    else if ([colorType isEqualToString:@"fontColor"])
    {
        if (_myNote.fontColor != nil)
        myWords = [_myNote.fontColor componentsSeparatedByString:@" "];
        else
            myWords = [[NSArray alloc] initWithObjects:@"0",@"0",@"0",nil];

    }
    
    NSString *redS = (NSString *)myWords[0];
    int red = redS.intValue;
    NSString *greenS = (NSString *)myWords[1];
    int green = greenS.intValue;
    NSString *blueS = (NSString *)myWords[2];
    int blue = blueS.intValue;
    
    UIColor *color = [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1];
    return color;
}

@end
