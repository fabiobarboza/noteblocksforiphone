//
//  FHNBChangeColorViewController.h
//  NoteBlocks
//
//  Created by Fabio Barboza on 2/27/14.
//  Copyright (c) 2014 Fabio Barboza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHNBBlock.h"
#import "FHNBNewNoteView.h"

@interface FHNBChangeColorViewController : UIViewController

@property (nonatomic) int currentModification;
@property (nonatomic) UIButton *buttonChanged;
@property (nonatomic) FHNBBlock *myBlock;
@property (nonatomic) Note *currentNote;

@property (nonatomic) FHNBNewNoteView *creationView;


@end
